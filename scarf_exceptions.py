class InvalidParameterException(Exception):

    def __init__(self, place, detail = "\b"):
        self.place = place
        self.detail = detail

    def __str__(self):
        return "The parameter of %s is invalid %s" % (self.place, self.detail)

class FailedGettingSkinException(Exception):

    def __init__(self, username):
        self.username = username

    def __str__(self):
        return "Failed to get skin for user %s" % self.username