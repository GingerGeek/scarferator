import scarf
import http.server as hserv
from urllib import parse

PORT = 6450
HOST = "0.0.0.0"

class RequestHandler(hserv.BaseHTTPRequestHandler):
    def do_GET(self):
        parsed = parse.urlparse(self.path)
        query = parse.parse_qs(parsed.query)

        if parsed.path == "/scarferator" or parsed.path == "/scarferator/":
            with open("./index.html", mode = "rt") as file:
                data = "".join(line for line in file)
                self.send_response(200)
                self.send_header("Content-type", "text/html")
                self.send_header("Content-length", len(data))
                self.end_headers()

                self.wfile.write(bytes(data, "ASCII"))
                return
            self.send_404("File: %s"%parsed.path)

        elif parsed.path == "/scarferator/get" or parsed.path == "/scarferator/get/":
            if "username" not in query or "tint" not in query:
                if "username" in query:
                    self.send_404("Please provide tint")
                    return
                if "tint" in query:
                    self.send_404("Please provide username")
                    return
                self.send_404("Please provide username and tint")
                return

            username = None;
            tint = None;
            try:
                username, tint = query["username"][0], query["tint"][0]
            except Exception:
                pass
            finally:
                if username == None or tint == None:
                    self.send_500("Failed to retrieve username")
                    return

            # Check if tint is correct
            try:
                int("0x" + tint, 16)
                if len(tint) != 6:
                    raise Exception("Invalid length")
            except ValueError as e:
                self.send_404("Please provide valid tint - format RRGGBB without # or alpha, in hexadecimal.<br>" + repr(e))
                return
            except Exception as e:
                self.send_404("Please provide valid tint - format RRGGBB without # or alpha, in hexadecimal.<br>" + repr(e))
                return
            tint = "#" + tint

            scarfed = None
            try:
                scarfed = scarf.make_scarf(username, tint)
            except FailedGettingSkinException:
                pass
            finally:
                if scarfed == None:
                    self.send_404("Please provide valid username. Skin cannot be found for user %s!" % username)
                    return

            self.send_response(200)
            self.send_header("Content-type", "image/png")
            self.send_header("Content-length", len(scarfed.getbuffer()))
            self.end_headers()

            self.wfile.write(scarfed.getbuffer())
        else:
            self.send_404("File: %s"%parsed.path)

    def send_404(self, more = None):
        response = b"404 - Document not found!"
        if more != None:
            response += b"<br>" + bytes(more, "ASCII")
        self.send_response(404)
        self.send_header("Content-type", "text/html")
        self.send_header("Content-length", len(response))
        self.end_headers()
        self.wfile.write(response)

    def send_500(self, more = None):
        response = b"500 - Internal server error!"
        if more != None:
            response += b"<br>" + bytes(more, "ASCII")
        self.send_response(500)
        self.send_header("Content-type", "text/html")
        self.send_header("Content-length", len(response))
        self.end_headers()
        self.wfile.write(response)

httpd = hserv.HTTPServer((HOST, PORT), RequestHandler)
print("Starting server on %s:%d" % (HOST, PORT))
httpd.serve_forever()