from urllib import request
from urllib.error import HTTPError
from PIL import Image
from PIL.ImageColor import getcolor, getrgb
from PIL.ImageOps import grayscale

from scarf_exceptions import InvalidParameterException, FailedGettingSkinException

import sys
import io

def image_tint(src, tint="#ffffff"):
    tint_image = Image.new(src.mode, src.size, tint)
    alpha = src.split()[-1]
    tint_image.putalpha(alpha)
    return Image.blend(src, tint_image, 0.5)

def make_scarf(username = "notrodash", tint_color="#fccf4a"):
    print("The username to be scarferated is %s with tint %s!" % (username, tint_color))

    try:
        global response
        response = request.urlopen("http://s3.amazonaws.com/MinecraftSkins/" + username + ".png")
    except HTTPError:
        print("Failed getting user skin")
        raise FailedGettingSkinException(username)

    skin = Image.open(io.BytesIO(response.read()))
    scarf = image_tint(Image.open("./scarf.png"), tint_color)

    skin.paste(scarf, (0, 0), scarf)
    img_io = io.BytesIO()
    skin.save(img_io, format="PNG")
    del skin
    del scarf
    img_io.seek(0)
    return img_io